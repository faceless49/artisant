import { ProductType } from 'redux/types';

const FILTER_NON_AVAILABLE = 0;

export const getFilteredUsers = (
  products: ProductType[],
  availableFilter: boolean,
): ProductType[] =>
  products.filter(
    ({ quantity_available: quantityAvailable }) =>
      !(availableFilter && quantityAvailable <= FILTER_NON_AVAILABLE),
  );
