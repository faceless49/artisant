import React, { ChangeEvent, Suspense, useCallback, useMemo, useState } from 'react';

import styles from './Home.module.scss';

import { Preloader } from 'features/Preloader';
import { Subtitle } from 'features/Subtitle';
import { getFilteredUsers } from 'helpers';
import { useAppSelector } from 'hooks/useAppSelector';
import { ProductType } from 'redux/types';
import { Nullable, ReturnComponentType } from 'types';

const ProductsList = React.lazy(() => import('components/ProductsList/'));

export const Home = (): ReturnComponentType => {
  const products = useAppSelector<ProductType[]>(state => state.products);

  const [isAvailable, setIsAvailable] = useState<boolean>(false);

  const filteredProducts = useMemo(
    (): ProductType[] => getFilteredUsers(products, isAvailable),
    [products, isAvailable],
  );

  const handleFilterIsAvailable = useCallback(
    (e: ChangeEvent<HTMLInputElement>): Nullable<void> => {
      setIsAvailable(e.currentTarget.checked);
    },
    [],
  );

  return (
    <section className={styles.home}>
      <div className={styles.home_wrapper}>
        <div className={styles.home_header}>
          <h2 className={styles.home_title}>Explore</h2>
          <Subtitle value="Buy and sell digital fashion NFT art" />
          <label htmlFor="available" className={styles.home_label}>
            In available
            <input
              name="available"
              type="checkbox"
              checked={isAvailable}
              onChange={handleFilterIsAvailable}
            />
          </label>
        </div>
        <div className={styles.home_products}>
          <Suspense fallback={<Preloader />}>
            <ProductsList products={filteredProducts} />
          </Suspense>
        </div>
      </div>
    </section>
  );
};
