import { ProductType } from 'redux/types';

export type ResponseType = {
  status: 'success' | 'failed';
  data: { products: ProductType[] };
};
