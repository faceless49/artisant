import { instance } from './apiConfig';
import { ResponseType } from './types/type';

export const productsAPI = {
  getProducts() {
    return instance.get<ResponseType>('/products');
  },
};
