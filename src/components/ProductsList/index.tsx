import React, { FC, useEffect } from 'react';

import { useDispatch } from 'react-redux';

import { Product } from 'components/Product';
import { fetchProducts } from 'redux/thunks/products';
import { ProductType } from 'redux/types';
import { ReturnComponentType } from 'types';

type ProductsListPropsType = {
  products: ProductType[];
};

const ProductsList: FC<ProductsListPropsType> = React.memo(
  ({ products }): ReturnComponentType => {
    const dispatch = useDispatch();

    useEffect(() => {
      dispatch(fetchProducts());
    }, []);

    return (
      <>
        {products.map(
          ({
            product_id: id,
            initial_price: initialPrice,
            created_by: createdBy,
            name,
            quantity_available: quantityAvailable,
          }) => (
            <Product
              key={id}
              product_id={id}
              name={name}
              initial_price={initialPrice}
              currency="ETH"
              created_by={createdBy}
              quantity_available={quantityAvailable}
            />
          ),
        )}
      </>
    );
  },
);
export default ProductsList;
