import React, { FC } from 'react';

import { Link } from 'react-router-dom';

import styles from './Product.module.scss';

import { Image } from 'components/Image';
import { ProductType } from 'redux/types';
import { ReturnComponentType } from 'types';

type ProductPropsType = ProductType & {
  currency: 'ETH' | 'BTC' | 'USDT';
};

export const Product: FC<ProductPropsType> = React.memo(
  ({
    initial_price: initialPrice,
    created_by: createdBy,
    name,
    product_id: id,
    currency,
    quantity_available: quantityAvailable,
  }): ReturnComponentType => (
    <div className={styles.product_inner}>
      <Link className={styles.product_link} to={`/product/${id}`}>
        <div className={styles.product_card}>
          <Image />
          <div className={styles.product_card_overlay}>
            <h3 className={styles.product_created}>created by</h3>
            <h3 className={styles.product_author}>{createdBy.display_name}</h3>
            <div className={styles.product_item}>
              <span>{name}</span>
            </div>
          </div>
        </div>
        <div className={styles.product_footer}>
          <div className={styles.product_quantity}>
            <span className={styles.product_available}>available</span>
            {quantityAvailable} of 50
          </div>
          <div className={styles.product_price_wrapper}>
            <span className={styles.product_price}>price</span>
            <span className={styles.product_currency}>
              {initialPrice} {currency}
            </span>
          </div>
        </div>
      </Link>
    </div>
  ),
);
