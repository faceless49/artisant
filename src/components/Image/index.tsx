import styles from './Image.module.scss';

import img from 'assets/image.png';
import { ReturnComponentType } from 'types';

export const Image = (): ReturnComponentType => (
  <img src={img} alt="avatar" className={styles.img} />
);
