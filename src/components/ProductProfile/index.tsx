import React from 'react';

import { useNavigate, useParams } from 'react-router-dom';

import styles from './ProductProfile.module.scss';

import { Subtitle } from 'features/Subtitle';
import { useAppSelector } from 'hooks/useAppSelector';
import { ProductType } from 'redux/types';
import { Nullable, ReturnComponentType } from 'types';

export const ProductProfile = (): ReturnComponentType => {
  const { productId } = useParams();
  const navigate = useNavigate();

  const handlePrevPageButtonClick = (): Nullable<void> => {
    navigate('/');
  };
  const product = useAppSelector<ProductType>(
    state =>
      state.products.find(
        ({ product_id: id }) => id === Number(productId),
      ) as ProductType,
  );

  return (
    <div className={styles.profile}>
      <div className={styles.profile_header}>
        <Subtitle value={`Профиль пользователя ${product.created_by.display_name}`} />
        <div style={{ fontSize: 25 }}> Example...</div>
        <button
          className={styles.profile_btn}
          onClick={handlePrevPageButtonClick}
          type="button"
        >
          Вернуться на главную
        </button>
      </div>
    </div>
  );
};
