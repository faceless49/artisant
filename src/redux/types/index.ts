export type ProductType = {
  product_id: number;
  initial_price: number;
  name: string;
  created_by: OwnerType;
  quantity_available: number;
};

type OwnerType = {
  display_name: string;
};
