import { createSlice } from '@reduxjs/toolkit';

import { fetchProducts } from 'redux/thunks/products';
import { ProductType } from 'redux/types';

const slice = createSlice({
  name: 'products',
  initialState: [] as ProductType[],
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchProducts.fulfilled, (state, action) =>
      action.payload.map(product => product),
    );
  },
});

export const productsReducer = slice.reducer;
