import { createAsyncThunk } from '@reduxjs/toolkit';

import { productsAPI } from 'api/api';

export const fetchProducts = createAsyncThunk(
  'artisant/app/products',
  async (param, { rejectWithValue }) => {
    try {
      const res = await productsAPI.getProducts();
      if (res.data.status === 'success') {
        const { products } = res.data.data;
        products.length = 15; // its bad practice but it is test task.
        return products;
      }
      return rejectWithValue({});
    } catch (error) {
      console.warn(error);
      return rejectWithValue({});
    }
  },
);
