import React from 'react';

import { AppRoutes } from 'Routes/AppRoutes';
import { ReturnComponentType } from 'types';
import './index.scss';

const App = (): ReturnComponentType => (
  <div>
    <div className="container">
      <AppRoutes />
    </div>
  </div>
);

export default App;
